<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api'], function () {
    Route::get('login/{provider_request}', 'SocialiteController@redirectToProvider');
    Route::get('login/{provider_request}/callback', 'SocialiteController@handleProviderCallback');
    
    Route::group(['middleware' => ['apiJwt']], function () {

        Route::get('logout', 'SocialiteController@logoutProvider');

        // Route::get('user', 'UserController@index');
        // Route::post('user','UserController@store');
        Route::patch('user','UserController@update');
        // Route::delete('user/{user}','UserController@destroy');
        Route::get('user_information_fields', 'UserController@getUserInformationFields');

        Route::get('products_detail', 'ProductController@index');
        Route::post('products_detail','ProductController@store');
        // Route::patch('products_detail/{product}','ProductController@update');
        // Route::delete('products_detail/{product}','ProductController@destroy');
        Route::get('products_structure/{type}', 'ProductController@structure');

        Route::get('ratings', 'RatingController@index');
        Route::post('ratings','RatingController@store');
        // Route::patch('ratings/{rating}','RatingController@update');
        // Route::delete('ratings/{rating}','RatingController@destroy');
        
        Route::get('user_products_ratings', 'RatingCalculationController@getUserProductsRatings');
        Route::get('user_products_average_ratings', 'RatingCalculationController@getUserProductsAverageRatings');

    });
});