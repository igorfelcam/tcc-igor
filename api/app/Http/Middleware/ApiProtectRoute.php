<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiProtectRoute extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $ex) {
            if ($ex instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return new JsonResponse(
                    [
                        'status'    => false,
                        'data'      => [],
                        'message'   => "Token is Invalid"
                    ]
                );
            }
            elseif ($ex instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return new JsonResponse(
                    [
                        'status'    => false,
                        'data'      => [],
                        'message'   => "Token is Expired"
                    ]
                );
            }
            else {
                return new JsonResponse(
                    [
                        'status'    => false,
                        'data'      => [],
                        'message'   => "Authorization Token not found"
                    ]
                );
            }
        }

        return $next($request);
    }
}
