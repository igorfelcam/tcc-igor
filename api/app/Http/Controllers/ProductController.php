<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Product;
use App\ProductStructure;

class ProductController extends Controller
{
    private $product;
    private $productStructure;
    private $request;
    private $jwtAuth;

    public function __construct(Product $product, ProductStructure $productStructure, Request $request, JWTAuth $jwtAuth)
    {
        $this->product          = $product;
        $this->productStructure = $productStructure;
        $this->request          = $request;
        $this->jwtAuth          = $jwtAuth;
    }

    /**
     * Obtain all products details.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product::all();
        $data = $products->map(function ($product) {
            return [
                'brand'     => $product->brand,
                'model'     => $product->model,
                'version'   => $product->version
            ];
        });

        return new JsonResponse(
            [
                'status'    => true,
                'data'      => $data,
                'message'   => "All products"
            ]
        );
    }
    
    /**
     * Obtain structure producture by type.
     *
     * @param String $type
     * @return \Illuminate\Http\Response
     */
    public function structure($type)
    {
        try {
            $product_structure = $this->productStructure::query()->firstOrNew(['type' => strtolower($type)]);

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => json_decode($product_structure->structure),
                    'message'   => "Product structure to $type"
                ]
            );
        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error product store: ". $ex->getMessage()
                ]
            );
        }

    }

    /**
     * Save new product detail.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $brand      = strtolower($this->request->brand);
            $model      = strtolower($this->request->model);
            $version    = strtolower($this->request->version);

            if (empty($brand) || empty($model)) {
                throw new \Exception("Empty fields");
            }

            $product = $this->product::query()->firstOrNew(
                [
                    'brand'     => $brand,
                    'model'     => $model,
                    'version'   => $version
                ]
            );

            if (!$product->exists) {
                $edition_user       = $this->jwtAuth::user();
                $type               = "smartphone";
                $product_structure  = $this->productStructure::where('type', $type)->get()->first();

                $product->product_structure_id  = $product_structure->id;
                $product->brand                 = $brand;
                $product->model                 = $model;
                $product->version               = $version;
                $product->edition_user_id       = $edition_user->id;

                if (!$product->save()) {
                    throw new \Exception("Product Registration Failed");
                }
            }

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => [
                        'brand'     => $product->brand,
                        'model'     => $product->model,
                        'version'   => $product->version
                    ],
                    'message'   => "Created new product"
                ]
            );

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error product store: ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Save product update.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        return;
    }

    /**
     * Remove the product.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $this->product->delete();
        return;
    }
}
