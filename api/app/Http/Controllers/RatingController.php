<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\ProductUser as Rating;
use App\Product;
use App\ProductStructure;

class RatingController extends Controller
{
    private $product;
    private $productStructure;
    private $rating;
    private $request;
    private $jwtAuth;

    public function __construct(Product $product, ProductStructure $productStructure, Rating $rating, Request $request, JWTAuth $jwtAuth)
    {
        $this->product          = $product;
        $this->productStructure = $productStructure;
        $this->rating           = $rating;
        $this->request          = $request;
        $this->jwtAuth          = $jwtAuth;
    }

	/**
     * Obtain all ratings.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
        $product_users = $this->rating::all();
        $data = $product_users->map(function ($product_user) {
            return [
                'user_id'	=> $product_user->user_id,
                'rating'	=> json_decode($product_user->rating)
            ];
        });

        return new JsonResponse(
            [
                'status'    => true,
                'data'      => $data,
                'message'   => "All ratings"
            ]
        );
    }

    /**
     * Save new rating.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $user = $this->jwtAuth::user();
            
            $brand      = strtolower($this->request->brand);
            $model      = strtolower($this->request->model);
            $version    = strtolower($this->request->version);

            $product = $this->product::query()->firstOrNew(
                [
                    'brand'     => $brand,
                    'model'     => $model,
                    'version'   => $version
                ]
            );

            if (!$product->exists) {
                throw new \Exception("Product not found.");
            }

            $rating = $this->rating::query()->firstOrNew(
                [
                    'user_id'       => $user->id,
                    'product_id'    => $product->id
                ]
            );

            $this->structureValidate($product->product_structure_id, json_decode($this->request->rating));

            $rating->rating     = $this->request->rating;
            $response_message   = "Updated rating";

            if (!$rating->exists) {
                $rating->user_id    = $user->id;
                $rating->product_id = $product->id;
                
                $response_message   = "Created new rating";
            }

            if (!$rating->save()) {
                throw new \Exception("Product Registration Failed");
            }

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => [
                        'email'     => $user->email,
                        'brand'     => $brand,
                        'model'     => $model,
                        'version'   => $version,
                        'rating'    => json_decode($rating->rating)
                    ],
                    'message'   => $response_message
                ]
            );

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error rating store: ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Save rating update.
     *
     * @param Object $rating
     * @param Object $request
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        return;
    }

    /**
     * Remove the rating.
     *
     * @param Object $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $this->rating->delete();
        return;
    }

    /**
     * Validate the rating structure.
     *
     * @param Integer $product_structure_id
     * @param Object $request_rating
     * @return Boolean $valid
     */
    private function structureValidate($product_structure_id, $request_rating)
    {
        $product_structure  = $this->productStructure::where('id', $product_structure_id)->get()->first();
        $product_structure  = json_decode($product_structure->structure);
        
        $request_rating->popular = (Array) $request_rating->popular;
        
        if (count($product_structure->popular) !== count($request_rating->popular)) {
            throw new \Exception("Product structure length invalid.");
        }

        foreach ($product_structure->popular as $key => $structure) {
            $is_valid = array_key_exists($structure, $request_rating->popular);

            if (!$is_valid) {
                throw new \Exception("Product structure invalid.");
            }
        }

        return true;
    }

}
