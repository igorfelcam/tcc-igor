<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialiteController extends Controller
{
    const FACEBOOK_PROVIDER = 'facebook';
    const GOOGLE_PROVIDER   = 'google';

    private $user;
    private $socialite;
    private $jwtAuth;

    public function __construct(User $user, Socialite $socialite, JWTAuth $jwtAuth)
    {
        $this->user         = $user;
        $this->socialite    = $socialite;
        $this->jwtAuth      = $jwtAuth;
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param String $provider_request
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider_request)
    {
        try {
            $validated_provider = $this->validateProvider($provider_request);

            return $this->socialite::driver($validated_provider)
                ->with(['social_type' => $provider_request])
                ->stateless()
                ->redirect()
                ->getTargetUrl();

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error redirect to provider: ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Obtain the user information from provider.
     *
     * @param String $provider_request
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider_request)
    {
        try {
            $validated_provider = $this->validateProvider($provider_request);
            $providerUser       = $this->socialite::driver($validated_provider)->stateless()->user();
            $user               = $this->user::query()->firstOrNew(['email' => $providerUser->getEmail()]);
    
            if (!$user->exists) {
                $user->name     = $providerUser->getName();
                $user->avatar   = $providerUser->getAvatar();
    
                if (!$user->save()) {
                    throw new \Exception("User Registration Failed");
                }
            }
    
            $token = $this->jwtAuth::fromUser($user);
            if (!$token) {
                throw new \Exception("Unauthorized");
            }

            $data = [
                'access_token'  => $token,
                'token_type'    => 'bearer',
                'expires_in'    => auth('api')->factory()->getTTL() * 60,
                'user_name'     => $user->name,
                'user_avatar'   => $user->avatar
            ];

            $url = env('CALLBACK_FRONT') ."?".
                "access_token=". $data['access_token'] .
                "&token_type=". $data['token_type'] .
                "&expires_in=". $data['expires_in'] .
                "&user_name=". $data['user_name'] .
                "&user_avatar=". $data['user_avatar'];

            $url = trim($url);
            
            return redirect($url);

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error handle provider callback: ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Obtain the token JWT.
     *
     * @param String $provider
     * @return String $provider
     */
    private function validateProvider($provider)
    {
        if (
            $provider != self::FACEBOOK_PROVIDER &&
            $provider != self::GOOGLE_PROVIDER
        ) {
            throw new \Exception("Provider not found");
        }

        return $provider;
    }

    /**
     * Obtain the token JWT.
     *
     * @param String $token
     * @return \Illuminate\Http\Response
     */
    private function respondWithToken($token)
    {
        return new JsonResponse(
            [
                'status'        => true,
                'data'          => [
                    'access_token'  => $token,
                    'token_type'    => "bearer",
                    'expires_in'    => auth('api')->factory()->getTTL() * 60
                ],
                'message'       => ""
            ]
        );
    }

    /**
     * Logout invalidate token JWT.
     *
     * @return \Illuminate\Http\Response
     */
    public function logoutProvider()
    {
        try {
            $this->jwtAuth::invalidate();

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => [],
                    'message'   => "Success logout, see u later!"
                ]
            );

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error logout: ". $ex->getMessage()
                ]
            );
        }
    }
}
