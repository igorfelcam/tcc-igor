<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;

class UserController extends Controller
{

    private $user;
    private $request;
    private $userJwt;

    public function __construct(User $user, Request $request, JWTAuth $userJwt)
    {
        $this->user     = $user;
        $this->request  = $request;
        $this->userJwt  = $userJwt;
    }

    /**
     * Get user information fields.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserInformationFields()
    {
        try {
            $user = $this->userJwt::user();

            if (!$user->exists) {
                throw new \Exception("User not found");
            }

            $empty_fields = $this->fields($user);
            $empty_fields = array_filter($empty_fields, fn($value) => $value === null || $value === '');

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => array_keys($empty_fields),
                    'message'   => "User fields empty"
                ]
            );

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error get user information ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Save user update.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        try {
            $user = $this->user->firstWhere('email', $this->userJwt::user()->email);

            if (!$user->exists) {
                throw new \Exception("User not found, didn't update");
            }
            
            $update_fields = $this->fields();
            $update_fields = array_filter($update_fields, fn($value) => $value !== null && $value !== '');

            if (!is_array($update_fields) || (is_array($update_fields) && count($update_fields) === 0)) {
                throw new \Exception("Fields not set, didn't update");
            }

            foreach ($update_fields as $key => $field) {
                $user->$key = $field;
            }

            if (!$user->save()) {
                throw new \Exception("User update Failed");
            }

            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => [
                        $update_fields
                    ],
                    'message'   => "Success user update"
                ]
            );

        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error user update ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Return user fields structure
     * 
     * @return Array $fields
     */
    private function fields($user = null)
    {
        if ($user === null) {
            $user = $this->request;
        }

        return [
            'avatar'            => $user->avatar,
            'age_range_min'     => $user->age_range_min,
            'age_range_max'     => $user->age_range_max,
            'location'          => $user->location,
            'gender'            => $user->gender,
            'birthday'          => $user->birthday,
            'job'               => $user->job,
            'education_level'   => $user->education_level
        ];
    }
}
