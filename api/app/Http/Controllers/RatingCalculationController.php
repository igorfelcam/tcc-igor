<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\ProductUser as Rating;
use App\Product;
use App\ProductStructure;

class RatingCalculationController extends Controller
{
    private $product;
    private $productStructure;
    private $rating;
    private $jwtAuth;

    public function __construct(Product $product, ProductStructure $productStructure, Rating $rating, JWTAuth $jwtAuth)
    {
        $this->product          = $product;
        $this->productStructure = $productStructure;
        $this->rating           = $rating;
        $this->jwtAuth          = $jwtAuth;
    }

    /**
     * Get user ratings.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserProductsRatings()
    {
        try {
            $user           = $this->jwtAuth::user();
            $user_ratings   = $this->rating::where('user_id', $user->id)->get();
    
            if (empty($user_ratings)) {
                $user_ratings = '';
            }

            $data = [];
            foreach ($user_ratings as $key => $user_rating) {
                $product = $this->product::find($user_rating->product_id);

                $data[] = [
                    'brand'     => $product->brand,
                    'model'     => $product->model,
                    'version'   => $product->version,
                    'rating'    => json_decode($user_rating->rating)
                ];
            }
    
            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => $data,
                    'message'   => "All ". $user->name ." ratings"
                ]
            );
        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error get user rating: ". $ex->getMessage()
                ]
            );
        }
    }
    
    /**
     * Get average user ratings.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserProductsAverageRatings()
    {
        try {
            $user           = $this->jwtAuth::user();
            $user_ratings   = $this->rating::where('user_id', $user->id)
                                ->orderBy('updated_at', 'desc')
                                ->get();
    
            if (empty($user_ratings)) {
                $user_ratings = '';
            }

            $result_average = [];
            foreach ($user_ratings as $key => $user_rating) {
                
                $product            = $this->product::find($user_rating->product_id);
                $product_ratings    = $this->getProductsRatings($product);
                $product_structure  = $this->productStructure::find($product->product_structure_id);

                $product_result = $this->averageProductsRating(
                    $product_structure,
                    $product_ratings
                );

                $result_average[] = [
                    'brand'             => $product->brand,
                    'model'             => $product->model,
                    'version'           => $product->version,
                    'structure_rating'  => json_decode($product_structure->structure),
                    'average_rating'    => $product_result
                ];
            }
    
            return new JsonResponse(
                [
                    'status'    => true,
                    'data'      => $result_average,
                    'message'   => "All ". $user->name ." ratings"
                ]
            );
        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'status'    => false,
                    'data'      => [],
                    'message'   => "Error get user rating: ". $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Get all ratings of the product.
     *
     * @param Object $product
     * @return Array $product_ratings
     */
    private function getProductsRatings($product)
    {
        $ratings = $this->rating::where('product_id', $product->id)->get();

        $product_ratings = [];
        foreach ($ratings as $key => $rating) {
            $product_ratings[] = json_decode($rating->rating);
        }

        return $product_ratings;
    }

    /**
     * Calculates average product ratings
     *
     * @param String $product_structure
     * @param Array $product_ratings
     * @return Array $structure
     */
    private function averageProductsRating($structure, $product_ratings)
    {
        $structure          = json_decode($structure);
        $product_structure  = (array) json_decode($structure->structure);
        $types_rating       = array_keys($product_structure);
        $divider            = count($product_ratings);

        if ($divider === 0) {
            throw new \Exception("Nothing product ratings, resulting division by zero");
        }

        $product_result = [];
        foreach ($types_rating as $key => $type_rating) {
                    
            $average_result = array_flip($product_structure[$type_rating]);
            $average_result = array_map(fn() => 0, $average_result);
    
            foreach ($product_ratings as $key => $product_rating) {
                foreach ($product_structure[$type_rating] as $key => $popular) {
                    $average_result[$popular] = $average_result[$popular] + $product_rating->popular->{$popular};
                }
            }
    
            foreach ($average_result as $key => &$split) {
                $split = $split / $divider;
            }

            $product_result += [$type_rating => $average_result];
        }

        return $product_result;
    }
}
