<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rating'
    ];

    /**
     * Get the user that owns the product user.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the product that owns the product user.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
