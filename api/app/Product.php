<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_structure_id',
        'brand',
        'model',
        'version',
        'edition_user_id'
    ];

    /**
     * Get the product users users for the product.
     */
    public function product_users()
    {
        return $this->hasMany('App\ProductUser');
    }

    /**
     * Get the product structure that owns the product.
     */
    public function product_structure()
    {
        return $this->belongsTo('App\ProductStructure');
    }
}
