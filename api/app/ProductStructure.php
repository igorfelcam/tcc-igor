<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStructure extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'structure'
    ];

    /**
     * Get the product users for the product structure.
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
