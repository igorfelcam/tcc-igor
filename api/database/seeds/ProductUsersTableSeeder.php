<?php

use Illuminate\Database\Seeder;

class ProductUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_users')->insert([
            'user_id'       => 1,
            'product_id'    => 1,
            'rating'        => '{"technical_knowledge":0,"popular":{"camera":0,"memory":0,"battery":0,"display":0,"design":0,"usability":0,"speed":0},"technical":{}}'
        ]);
    }
}
