<?php

use Illuminate\Database\Seeder;

class ProductStructuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_structures')->insert([
            'type'      => 'smartphone',
            'structure' => '{"popular":["camera","memory","battery","display","design","usability","speed"],"technical":[]}'
        ]);
    }
}
