<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'product_structure_id'  => 1,
            'brand'                 => 'iPhone',
            'model'                 => 'SE',
            'version'               => '128 GB'
        ]);
        
        DB::table('products')->insert([
            'product_structure_id'  => 1,
            'brand'                 => 'Samsung',
            'model'                 => 'Galaxy s20',
            'version'               => '128 GB'
        ]);

        DB::table('products')->insert([
            'product_structure_id'  => 1,
            'brand'                 => 'Motorola',
            'model'                 => 'Moto G8 Plus',
            'version'               => '64 GB'
        ]);
    }
}
