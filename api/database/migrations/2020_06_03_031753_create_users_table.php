<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('avatar', 100)->nullable();
            $table->integer('age_range_min')->nullable();
            $table->integer('age_range_max')->nullable();
            $table->string('location', 500)->nullable();
            $table->string('gender', 100)->nullable();
            $table->string('birthday', 100)->nullable();
            $table->string('job', 100)->nullable();
            $table->string('education_level', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
