# Igor's TCC

## GitLab-Flow

![GitLab Flow Image](https://qiita-user-contents.imgix.net/https%3A%2F%2Fqiita-image-store.s3.amazonaws.com%2F0%2F185389%2F68b5a27b-e32b-d61f-6020-96de3ba1d335.png?ixlib=rb-1.2.2&auto=format&gif-q=60&q=75&s=faa42669c215fbb152782c8a894abe81)

<i>Font:</i> [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)

## Branchs name
- production
- pre-Production
- master
- feature/<i>feature-short-name</i>
- hotfix/<i>hotfix-short-name</i>

## Commits messages

### Feature
```
feature/description
```

### Hotfix
```
hotfix/description
```

### Documentation
```
docs/description
```

## Versions releases

### New release
```
v1.0.0 - v2.0.0 - vN.0.0
```

### New feature
```
v1.1.0 - v1.2.0 - v1.N.0
```

### Fix feature
```
v1.1.1 - v1.1.2 - v1.1.N
```