import Vue from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';

Vue.config.productionTip    = false;
Vue.prototype.$http         = axios;
Vue.prototype.$rangeRating  = parseInt(process.env.VUE_APP_RATING_STARS_RANGE, 10);
Vue.prototype.$apiUrl       = process.env.VUE_APP_ROOT_API

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
