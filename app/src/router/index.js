import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import UserInformation from '../views/UserInformation.vue';
import ProductDetail from '../views/ProductDetail.vue';
import ProductRating from '../views/ProductRating.vue';
import UserRatings from '../views/UserRatings.vue';

Vue.use(VueRouter)

  const routes = [
  {
    path: '*',
    redirect: '/'
  },
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/user-information',
    name: 'UserInformation',
    component: UserInformation,
    meta: {
      requeresAuth: true
    }
  },
  {
    path: '/product-detail',
    name: 'ProductDetail',
    component: ProductDetail,
    meta: {
      requeresAuth: true
    }
  },
  {
    path: '/product-rating/:brand/:model/:version',
    name: 'ProductRating',
    component: ProductRating,
    params: true,
    meta: {
      requeresAuth: true
    }
  },
  {
    path: '/user-ratings',
    name: 'UserRatings',
    component: UserRatings,
    meta: {
      requeresAuth: true
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
